from ctypes import *

# Loading the shared object file
tools = CDLL('./tools.so')

num = 684
sum = tools.SumOfDigits(num)
print("Sum of digits of %s = %s" %(num, sum))
