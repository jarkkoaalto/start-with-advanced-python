// File name: tools.c
// Purpose  : Common functions to be used
// Author   : J Aalto
// Date     : 11 Novermber 2019

int SumOfDigits(int num);

int SumOfDigits(int num)
{
    int sum = 0;
    while(num > 0)
    {
        sum += num%10;
        num = num / 10;
    }

    return sum;
}