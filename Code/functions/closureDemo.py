def DoOperation(op):
    app = "NetHub"
    global version
    version = 1.0
    # inside function
    def Execute():
        global version
        print("Executing command: " + app + " " + str(version) + " " + op)
        version += 1

    return Execute

restoreOp = DoOperation("restore")

restoreOp()
restoreOp()

backup = DoOperation("backup")

backup()

'''
Executing command: NetHub 1.0 restore
Executing command: NetHub 2.0 restore
Executing command: NetHub 1.0 backup
'''