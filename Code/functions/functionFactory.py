def Operation(systemType):
    def ExecuteCommand(command):
        if(systemType == "Linux"):
            def ExecuteLinuxCommand(command):
                print("Executing " + command + " on " + systemType )
            return ExecuteLinuxCommand
        if(systemType == "Windows"):
            def ExecuteWindowsCommand(command):
                print("Executing " + command + " on " + systemType )
            return ExecuteWindowsCommand
    return ExecuteCommand

LinuxOperation = Operation("Linux")
WindowsOperation = Operation("Windows")

LinuxOperation("Restart")
WindowsOperation("Shutdown")