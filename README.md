# Start with Advanced Python #

- Development of Software Product
- Appropriate use of feature
- Capability of language
- Better solution

## What is Library ? ##

Library is a collection of functionality in form of code which can be reused.

### Implementation of Library ###
- Write the class and methods of functions in a python file

```
if __name__ = '__main__'
```
### Library Usage ###
- Import the library file which has class and methods to be used
- Use the library file name to access the class
- Have both the files in same directory or environment variable PYTHONPATH set to library path

### Library Operationlib ###
- Create a library for operation to do the operations backup, restore and update

### Design the Library
- Library Name - Operationlib
- File Name - Operationlib.py

```
Class - Operation
Backup() - To take the backup of data
Parameters - None
Return value - None
Restore() - To restore the software to the previous version 
Parametrs - None
Return Value - None
```

#### Usage ####
import Operationlib

oprObject = Operationlib.Operation()

### Python C Extensions ###
- Python procides the facility to interface with C code

. ctype
- Python /C Api
- SWIG
- Cyton
- Pyrex
- SIP
- Boost.Python

### Step for C Extension (ctypes) ###
- Write a C source file having function
- Write Python program to use C function
- Create .so from C file using gcc compiler
- Execute Python program

### Run Ctools ###
gcc -shared -fPIC tools.c -o

and 

python testtools.py

### Closure ###
- A closure is created when a function is available inside another function

### Function Factory ###
- A function factory is used to create function objects.
